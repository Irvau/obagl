#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <GL/glew.h>

#include "oBagl.h"
#include "oBagl/Shader.h"
#include "oBagl/File.h"

unsigned int CurrShader;

void oBaUseShader(unsigned int Shader){
	if(CurrShader != Shader){
		CurrShader = Shader;
		glUseProgram((GLuint)Shader);
	}
}

unsigned int oBaCurrentShader(){
	return CurrShader;
}

unsigned int oBaLoadShaderFromFiles(const char *VertexPath,const char *FragmentPath){
	unsigned int ShaderProgram = 0;
	
	FILE *VertFile = fopen(VertexPath,"r");
	FILE *FragFile = fopen(FragmentPath,"r");
	
	if(VertFile && FragFile){
		size_t VertSize,FragSize;
		
		fseek(VertFile,0,SEEK_END);
		VertSize = ftell(VertFile);
		rewind(VertFile);
		
		fseek(FragFile,0,SEEK_END);
		FragSize = ftell(FragFile);
		rewind(FragFile);
		
		char *VertText = malloc(VertSize);
		char *FragText = malloc(FragSize);
		
		if(VertText && FragText){
			size_t VReadRes = fread(VertText,VertSize,1,VertFile);
			size_t FReadRes = fread(FragText,FragSize,1,FragFile);
			
			if(VReadRes == 1 && FReadRes == 1){
				ShaderProgram = oBaLoadShaderFromSources(VertText,FragText);
			}else if(oBaDebugOutput){
				if(VReadRes != 1){
					printf("Read of \"%s\" failed\n",VertexPath);
				}
				
				if(FReadRes != 1){
					printf("Read of \"%s\" failed\n",FragmentPath);
				}
			}
			
			free(VertText);
			free(FragText);
		}else if(oBaDebugOutput){
			if(!VertText){
				printf("Memory allocation for reading \"%s\" failed\n",VertexSource);
			}
			
			if(!FragText){
				printf("Memory allocation for reading \"%s\" failed\n",FragmentSource);
			}
		}
		
		fclose(FragFile);
		fclose(VertFile);
	}else if(oBaDebugOutput){
		if(!VertFile){
			printf("Unable to open \"%s\"\n",VertexPath);
		}else{
			fclose(VertFile);
		}
		
		if(!FragFile){
			printf("Unable to open \"%s\"\n",FragmentPath);
		}else{
			fclose(FragFile);
		}
	}
	
	return ShaderProgram;
}

unsigned int oBaLoadShaderFromSources(const char *VertexSource,const char *FragmentSource){
	GLuint ShaderProgram = 0;
	
	//Reading names
	char *VertexName = NULL,*FragmentName = NULL;
	
	if(oBaDebugOutput){
		if(!strncmp(VertexSource,"//",2)){
			size_t VertexLen = strlen(VertexSource);
			size_t NameEnd = 2;
			
			while(NameEnd < VertexLen && VertexSource[NameEnd] != '/' && VertexSource[NameEnd] != '\n' && VertexSource[NameEnd] != 0){
				++NameEnd;
			}
			
			VertexName = malloc(NameEnd * sizeof(char));
			
			for(size_t I = 0;I < NameEnd - 1;++I){
				VertexName[I] = VertexSource[I + 2];
			}
			
			VertexName[NameEnd - 1] = 0;
		}
		
		if(!strncmp(FragmentSource,"//",2)){
			size_t VertexLen = strlen(FragmentSource);
			size_t NameEnd = 2;
			
			while(NameEnd < VertexLen && FragmentSource[NameEnd] != '/' && FragmentSource[NameEnd] != '\n' && FragmentSource[NameEnd] != 0){
				++NameEnd;
			}
			
			FragmentName = malloc(NameEnd * sizeof(char));
			
			for(size_t I = 0;I < NameEnd - 1;++I){
				FragmentName[I] = FragmentSource[I + 2];
			}
			
			FragmentName[NameEnd - 1] = 0;
		}
	}
	
	//Compiling them
	GLuint Vert,Frag;
	GLint VertStatus,FragStatus;
	
	//Vertex
	Vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(Vert,1,&VertexSource,NULL);
	glCompileShader(Vert);
	
	glGetShaderiv(Vert,GL_COMPILE_STATUS,&VertStatus);
	
	if(oBaDebugOutput && !VertStatus){
		GLchar Log[512];
		glGetShaderInfoLog(Vert,512,NULL,Log);
		
		if(VertexName){
			printf("\"%s\": ",VertexName);
		}
		
		printf("Shader compilation failed:\n%s\n",Log);
	}
	
	//Fragment
	Frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(Frag,1,&FragmentSource,NULL);
	glCompileShader(Frag);
	
	glGetShaderiv(Frag,GL_COMPILE_STATUS,&FragStatus);
	
	if(oBaDebugOutput && !FragStatus){
		GLchar Log[512];
		glGetShaderInfoLog(Frag,512,NULL,Log);
		
		if(FragmentName){
			printf("\"%s\": ",FragmentName);
		}
		
		printf("Shader compilation failed:\n%s\n",Log);
	}
	
	//Linking
	if(VertStatus && FragStatus){
		GLint LinkStatus;
		
		ShaderProgram = glCreateProgram();
		glAttachShader(ShaderProgram,Vert);
		glAttachShader(ShaderProgram,Frag);
		glLinkProgram(ShaderProgram);
		
		glGetProgramiv(ShaderProgram,GL_LINK_STATUS,&LinkStatus);
		
		if(oBaDebugOutput){
			if(!LinkStatus){
				GLchar Log[512];
				glGetProgramInfoLog(ShaderProgram,512,NULL,Log);
				
				if(VertexName && FragmentName){
					printf("\"%s\" & \"%s\": ",VertexName,FragmentName);
				}
				
				printf("Shader linking failed:\n%s\n",Log);
			}else{
				printf("Successfully compiled shader");
				
				if(VertexName && FragmentName){
					printf(" with sources \"%s\" and \"%s\"",VertexName,FragmentName);
				}
				
				printf("\n");
			}
		}
		
		//Cleaning up some stuffs
		glDeleteShader(Vert);
		glDeleteShader(Frag);
		
		if(LinkStatus){
			//Binding the default UBOs, if they exist
			GLuint MIndex = glGetUniformBlockIndex(ShaderProgram,"oBa_Matrix");
			
			if(MIndex != GL_INVALID_INDEX){
				GLint UBOBinding;
				glGetActiveUniformBufferiv(ShaderProgram,LIndex,GL_UNIFORM_BLOCK_BINDING,&UBOBinding);
				
				if(UBOBinding != MATRIX_UBO){
					glUniformBlockBinding(ShaderProgram,MIndex,OBA_SHADER_MATRIX_UBO);
				}
			}
			
			GLuint LIndex = glGetUniformBlockIndex(ShaderProgram,"oBa_Lights");
			
			if(LIndex != GL_INVALID_INDEX){
				GLint UBOBinding;
				glGetActiveUniformBufferiv(ShaderProgram,LIndex,GL_UNIFORM_BLOCK_BINDING,&UBOBinding);
				
				if(UBOBinding != LIGHTS_UBO){
					glUniformBlockBinding(ShaderProgram,LIndex,OBA_SHADER_LIGHTS_UBO);
				}
			}
		}
	}
	
	//Ta-da!
	return ShaderProgram;
}

void oBaFreeShader(unsigned int Shader){
	glDeleteProgram(Shader);
}