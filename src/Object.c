#include "oBagl/Object.h"
#include "oBagl/Entity.h"

oBaObject oBaCreateObject(oBaTemplate Template,oBaEntity Entity){
	oBaObject Object = malloc(sizeof(oBafObject));
	
	if(Entity){
		Object->Entity = Entity;
	}else{
		Object->Entity = oBaCreateEntity();
	}
	
	Object->Template = Template;
	
	return Object;
}

void oBaFreeObject(oBaObject Object,char FreeEntity){
	if(FreeEntity){
		oBaFreeEntity(Object->Entity);
	}
	
	free(Object);
}