#include <stdarg.h>

#include <GL/glew.h>

#include "oBagl/Template.h"
#include "oBagl/Shader.h"

const char *TEX_UNIFORM_PREFIX = "oBa_Texture_";

char **TextureUniforms;

void oBaTemplateSetup(){
	TextureUniforms = malloc(OBA_MAX_TEMPLATE_TEXTURE_COUNT * sizeof(char*));
	size_t UniformPrefixSize = strlen(TEX_UNIFORM_PREFIX);
	
	for(size_t I = 0;I < OBA_MAX_TEMPLATE_TEXTURE_COUNT;++I){
		char IntChar;
		snprintf(&IntChar,1,"%u",I);
		
		TextureUniforms[I] = malloc((UniformPrefixSize + 2) * sizeof(char));
		strcpy(TextureUniforms[I],TEX_UNIFORM_PREFIX);
		strcat(TextureUniforms[I],IntChar);
	}
}

void oBaTemplateCleanUp(){
	for(size_t I = 0;I < OBA_MAX_TEMPLATE_TEXTURE_COUNT;++I){
		free(TextureUniforms[I]);
	}
	
	free(TextureUniforms);
}

oBaTemplate oBaCreateTemplate(){
	oBaTemplate Template = malloc(sizeof(struct oBaTemplate));
	
	Template->Mesh = NULL;
	
	for(size_t I = 0;I < OBA_MAX_TEMPLATE_TEXTURE_COUNT;++I){
		Template->Textures[I] = NULL;
	}
	
	Template->TexCount = 0;
	Template->Shader = 0;
	
	return Template;
}

void oBaTemplateTextures(oBaTemplate Template,int Count,...){
	va_list Args;
	Template->Count = Count - ((Count > OBA_MAX_TEMPLATE_TEXTURE_COUNT) * (Count - OBA_MAX_TEMPLATE_TEXTURE_COUNT));
	
	va_start(Args,Template->Count);
	
	for(size_t I = 0;I < Template->Count;++I){
		Template->Textures[I] = va_arg(Args,int);;
	}
	
	va_end(Args);
}

void oBaUseTemplate(oBaTemplate Template){
	for(size_t I = 0;I < Template->TexCount;++I){
		glActiveTexture(GL_TEXTURE0 + I);
		glBindTexture(GL_TEXTURE_2D,oBaTextureName(Template->Textures[I]));
		glUniform1i(glGetUniformLocation(oBaCurrentShader(),TextureUniforms[I],I));
	}
	
	if(Template->Shader > 0){
		oBaUseShader(Template->Shader);
	else{
		oBaUseShader(oBaDefaultShader());
	}
}