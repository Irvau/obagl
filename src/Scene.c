#include "oBagl/Scene.h"
#include "oBagl/Object.h"

struct oBafScene{
	oBaObject *Objects;
	unsigned int ObjsUsed,ObjsReserved;
	
	kmVec4 Lights[OBA_SCENE_MAX_LIGHTS];
	unsigned int LightsUBO,UsedLights;
}

oBaTemplate *LastTemplate = NULL;

oBaScene oBaCreateScene(){
	oBaScene Scene = malloc(sizeof(struct oBafScene));
	
	Scene->Objects = malloc(sizeof(oBaObject));
	Scene->ObjsUsed = 0;
	Scene->ObjsReserved = 1;
	
	for(size_t I = 0;I < MAX_LIGHT_COUNT;++I){
		Lights[I].w = -1.0;
	}
	
	glGenBuffers(1,&Scene->LightsUBO);
	
	glBindBuffer(GL_UNIFORM_BUFFER,(Gluint)Scene->LightsUBO);
	glBufferData(GL_UNIFORM_BUFFER,OBA_SCENE_MAX_LIGHTS * sizeof(kmVec4),NULL,GL_STATIC_DRAW);
	
	glBindBuffer(GL_UNIFORM_BUFFER,0);
	
	Scene->UsedLights = 0;
	
	return Scene;
}

void oBaFreeScene(oBaScene Scene){
	
}

void oBaSceneAddObject(oBaScene Scene,oBaObject Object){
	if(Scene->ObjsUsed == Scene->ObjsReserved){
		Scene->ObjsReserved *= 2;
		Scene->Objects = realloc(Scene->Objects,Scene->ObjsReserved);
	}
	
	oBaTemplate PreviousTemplate = NULL;
	char DunDid = 0;
	
	for(size_t I = 0;I < Scene->ObjsReserved && !DunDid;++I){
		if(Scene->Objects[I] && PreviousTemplate == Object->Template && Scene->Objects[I]->Template != Object->Template){
			memmove(Scene->Objects[I + 1],Scene->Objects[I],Scene->ObjsUsed - I);
			Scene->Objects[I] = NULL;
		}
		
		if(!Scene->Objects[I]){
			Scene->Objects[I] = Object;
			++Scene->ObjsUsed;
			DunDid = 1;
		}
		
		if(Scene->Objects[I]){
			PreviousTemplate = Scene->Objects[I]->Template;
	}
}

oBaObject oBaSceneCreateObject(oBaScene Scene,oBaTemplate Template = NULL,oBaEntity Entity = NULL){
	oBaObject Object = oBaCreateObject(Template,Entity);
	oBaSceneAddObject(Scene,Object);
	
	return Object;
}

void oBaSceneRemoveObject(oBaScene Scene,oBaObject Object){
	for(size_t I = 0;I < Scene->ObjsUsed;++I){
		if(Scene->Objects[I] == Object){
			memmove(Scene->Objects[I],Scene->Objects[I + 1],Scene->ObjsUsed - I);
			Scene->Objects[Scene->ObjsUsed] == NULL;
		}
	}
	
	--Scene->ObjsUsed;
}

void oBaSceneDestroyObject(oBaScene Scene,oBaObject Object){
	oBaSceneRemoveObject(Scene,Object);
	oBaFreeObject(Object);
}

void oBaDrawScene(oBaScene Scene){
	glBindBufferBase(GL_UNIFORM_BUFFER,OBA_SHADER_LIGHTS_UBO,Scene->LightsUBO);
	
	size_t Count = 0;
	
	for(size_t I = 0;I < Scene->ObjsReserved && Count < Scene->ObjsUsed;++I){
		if(Scene->Objects[I] && Scene->Objects[I]->Template){
			if(Scene->Objects[I]->Template != LastTemplate){
				LastTemplate = Scene->Objects[I]->Template;
				oBaUseTemplate(Scene->Objects[I]->Template);
			}
			
			oBaDrawMesh(Scene->Objects[I]->Template->Mesh);
			
			++Count;
		}
	}
}