#include <algorithm>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <png.h>

#include "oBagl.h"
#include "oBagl/Texture.h"
#include "oBagl/Utils.h"

struct oBafTexture{
	unsigned int Name,Width,Height;
};

oBaTexture oBaCreateTexture(unsigned int Width,unsigned int Height){
	oBaTexture Texture = malloc(sizeof(struct oBafTexture));
	
	glGenTextures(1,&(GLuint)Texture->Name);
	glBindTexture(GL_TEXTURE_2D,(GLuint)Texture->Name);
	
	/*glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);*/
	
	Texture->Width = Width;
	Texture->Height = Height;
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,(GLuint)Texture->Width,(GLuint)Texture->Height,0,GL_RGBA,GL_UNSIGNED_BYTE,0);
	
	if(oBaDebugOutput){
		printf("Created a %u x %u texture\n",Texture->Width,Texture->Height);
	}
}

oBaTexture oBaLoadTexture(const char *Path){
	oBaTexture Texture = NULL;
	
	//Opening the file
	FILE *PNGFile = fopen(Path,"rb");
	
	if(PNGFile){
		//Checking the file signature
		png_byte PNGSig[8];
		fread(PNGSig,1,8,PNGFile);
		
		if(!png_sig_cmp(PNGSig,0,8)){
			//Reading a buncha infos
			png_structp PNGReadStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);
			
			if(PNGReadStruct){
				png_infop PNGInfoStruct = png_create_info_struct(PNGReadStruct);
				
				if(PNGInfoStruct){
					png_init_io(PNGReadStruct,PNGFile);
					
					png_set_sig_bytes(PNGReadStruct,8);
					png_read_info(PNGReadStruct,PNGInfoStruct);
					
					png_uint_32 ImageWidth,ImageHeight,Channels;
					int BitDepth,ColorType;
					png_get_IHDR(PNGReadStruct,PNGInfoStruct,&ImageWidth,&ImageHeight,&BitDepth,&ColorType,NULL,NULL,NULL);
					Channels = png_get_channels(PNGReadStruct,PNGInfoStruct);
					
					//Converting the image to something that is suitable for our uses. The following code totally wasn't copy-pasted from a tutorial. Nuh-uh. No way.
					//Okay, I confess, it was.
					switch(ColorType){
						case PNG_COLOR_TYPE_PALETTE:
							png_set_palette_to_rgb(PNGReadStruct);
							Channels = 3;
							
							break;
						case PNG_COLOR_TYPE_GRAY:
							if(BitDepth < 8){
								png_set_expand_gray_1_2_4_to_8(PNGReadStruct);
							}
							
							BitDepth = 8;
							
							break;
					}
					
					if(png_get_valid(PNGReadStruct,PNGInfoStruct,PNG_INFO_tRNS)){
						png_set_tRNS_to_alpha(PNGReadStruct);
						++Channels;
					}
					
					if(BitDepth == 16){
						png_set_strip_16(PNGReadStruct);
					}
					
					png_read_update_info(PNGReadStruct,PNGInfoStruct);
					
					//To read!
					png_bytep* Rows = malloc(ImageHeight * sizeof(png_bytep));
					const unsigned int Stride = ImageWidth * BitDepth * Channels / 8;
					
					char* ImageData = malloc(ImageHeight * Stride * sizeof(char));
					
					for(size_t I = 0;I < ImageHeight;++I){
						Rows[I] = (png_bytep)ImageData + ((ImageHeight - I - 1) * Stride); //Note the reversal of the image rows, flipping it so that shaders don't have a wonky "1.0 - UV.y"
					}
					
					png_read_image(PNGReadStruct,Rows);
					
					free(Rows);
					
					//Passing the image data to the GPU
					Texture = malloc(sizeof(struct oBafTexture));
					
					glGenTextures(1,&Texture->Name);
					glBindTexture(GL_TEXTURE_2D,(GLuint)Texture->Name);
					
					GLenum Format;
					
					if(Channels == 3){
						Format = GL_RGB;
					}else if(Channels == 4){
						Format = GL_RGBA;
					}
					
					glTexImage2D(GL_TEXTURE_2D,0,Format,(GLuint)ImageWidth,(GLuint)ImageHeight,0,Format,GL_UNSIGNED_BYTE,(GLvoid*)ImageData);
					glBindTexture(GL_TEXTURE_2D,0);
					
					free(ImageData);
					
					//Some info updates
					Texture->Width = ImageWidth;
					Texture->Height = ImageHeight;
					
					//Ta-da!
					if(oBaDebugOutput){
						printf("Loaded texture \"%s\"\n",Path);
					}
				}
				
				png_destroy_read_struct(&PNGReadStruct,(png_infopp)0,(png_infopp)0);
			}
		}else if(oBaDebugOutput){
			printf("\"%s\" is not a valid png file\n",Path);
		}
	}else if(oBaDebugOutput){
		printf("Unable to open file \"%s\"\n",Path);
	}
	
	return Texture;
}

void oBaFreeTexture(oBaTexture Texture){
	glDeleteTextures(1,&Texture->Name);
	free(Texture);
}

unsigned int oBaTextureName(oBaTexture Texture){
	return Texture->Name;
}

unsigned int oBaTextureWidth(oBaTexture Texture){
	return Texture->Width;
}

unsigned int oBaTextureHeight(oBaTexture Texture){
	return Texture->Height;
}