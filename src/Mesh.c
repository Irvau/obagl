#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GL/glew.h>

#include "oBagl.h"
#include "oBagl/Mesh.h"

struct oBafVertexAttribute{
	unsigned int Size,StateCount;
	GLfloat *Data;
};

struct oBafMesh{
	GLuint VAO,*VBOs,EBO;
	
	unsigned int AttribCount;
	oBaVertexAttribute *Attribs;
	
	unsigned int AttribVertCount,VertCount;
};

typedef oBafVertexAttribute *oBaVertexAttribute;

GLuint CurrVAO = 0;

oBaMesh *oBaLoadMeshes(const char *Path,unsigned int ObjectCount){
	FILE *MeshFile = fopen(Path,"rb");
	
	if(MeshFile){
		//Checking the sig
		uint8_t Sig[4];
		
		fread(Sig,sizeof(uint8_t),4,MeshFile);
		
		if(!strcmp(Sig,"oBaO")){
			//Reading the info byte
			uint8_t Infos;
			fread(&Infos,sizeof(uint8_t),1,MeshFile);
			
			uint8_t DrawMode = (Infos >> 4) % 6;
			uint8_t AttribCount = (Infos & 0x0F) + 1;
			
			//Reading the attribute sizes
			uint8_t AttribSizes[AttribCount];
			uint8_t AttribSizeByteCount = ceil(AttribCount / 4);
			uint8_t AttribSizeBytes[AttribSizeByteCount];
			
			fread(AttribSizeBytes,sizeof(uint8_t),AttribSizeByteCount,MeshFile);
			
			for(size_t I = 0;I < AttribSizeByteCount;++I){
				for(size_t J = 0;J < AttribCount - (I * 4) && J < 4;++J){
					AttribSizes[I * 4 + J] = AttribSizeBytes[I] | (0xFF << (J * 2) >> 6);
				}
			}
			
			//Reading in the objects
			uint32_t ReadObjCount;
			fread(&ReadObjCount,sizeof(uint32_t),1,MeshFile);
			
			if(ObjectCount && ObjectCount < ReadObjCount){
				ReadObjCount = ObjectCount;
			}
			
			oBaMesh *Meshes = malloc(sizeof(struct oBafMesh) * ReadObjCount);
			
			for(size_t I = 0;I < ReadObjCount;++I){
				//Reading in some properties.
				Meshes[I].AttribCount = AttribCount;
				Meshes[I].Attribs = malloc(sizeof(oBafVertexAttribute) * AttribCount);
				
				fread(&Meshes[I].AttribVertCount,sizeof(uint32_t),1,MeshFile);
				
				for(size_t J = 0;J < AttribCount;++J){
					Meshes[I].Attribs[J].Size = AttribSizes[J];
					
					fread(&Meshes[I].Attribs[J].StateCount,sizeof(uint32_t),1,MeshFile);
					
				}
				
				//Meshes[I].
			}
		}else if(oBaDebugOutput){
			printf("Mesh file \"%s\" is not a valid .om file.\n",Path);
		}
		
		fclose(Meshfile);
	}else if(oBaDebugOutput){
		printf("Unable to open file \"%s\"\n",Path);
	}
}

oBaMesh oBaCreateMesh(char Formatting,float *VertData,unsigned int VertCount,float *IndiceData,unsigned int IndiceCount){
	oBaMesh Mesh = NULL;
	
	if((Format == OBA_MESH_2D || Format == OBA_MESH_3D) && VertData && VertCount){
		Mesh = malloc(sizeof(struct oBafMesh));
		
		unsigned int VertSize = ((Format == OBA_MESH_2D) * 4) + ((Format == OBA_MESH_3D) * 8);
		
		glGenVertexArrays(1,&Mesh->VAO);
		glBindVertexArray((GLuint)Mesh->VAO);
		
		unsigned int GLVertDataSize = VertCount * VertSize;
		GLfloat GLVertData[GLVertDataSize];
		
		for(size_t I = 0;I < GLVertDataSize;++I){
			GLVertData[I] = (GLfloat)VertData[I];
		}
		
		glGenBuffers(1,&Mesh->VBO);
		glBindBuffer(GL_ARRAY_BUFFER,(GLuint)Mesh->VBO);
		glBufferData(GL_ARRAY_BUFFER,(GLuint)GLVertDataSize * sizeof(GLfloat),GLVertData,GL_STATIC_DRAW);
		
		Mesh->Count = VertCount;
		
		if(Indices && IndiceCount){
			unsigned int GLIndiceDataSize = IndiceCount * 3;
			GLuint GLIndiceData[GLIndiceDataSize];
			
			for(size_t I = 0;I < GLIndiceDataSize;++I){
				GLIndiceData[I] = (GLuint)IndiceData[I];
			}
			
			glGenBuffers(1,&Mesh->EBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,(GLuint)Mesh->EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER,(GLuint)GLIndiceDataSize * sizeof(GLuint),GLIndiceData,GL_STATIC_DRAW);
			
			Mesh->Count = IndiceCount;
		}else{
			Mesh->EBO = 0;
		}
		
		if(Format == OBA_MESH_2D){
			glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,4 * sizeof(GLfloat),(GLvoid*)0);
			glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,4 * sizeof(GLfloat),(GLvoid*)(2 * sizeof(GLfloat)));
		}else if(Format == OBA_MESH_3D){
			glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,8 * sizeof(GLfloat),(GLvoid*)0);
			glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,8 * sizeof(GLfloat),(GLvoid*)(3 * sizeof(GLfloat)));
			glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,8 * sizeof(GLfloat),(GLvoid*)(5 * sizeof(GLfloat)));
			
			glEnableVertexAttribArray(2);
		}
		
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	}
	
	return Mesh;
}

void oBaFreeMesh(oBaMesh Mesh){
	glDeleteVertexArrays(1,&Mesh->VAO);
	glDeleteBuffers(1,&Mesh->VBO);
	glDeleteBuffers(1,&Mesh->EBO);
	
	free(Mesh);
}

void oBaDrawMesh(oBaMesh Mesh){
	if(Mesh->VAO && Mesh->VBO){
		if(Mesh->VAO != CurrVAO){
			glBindVertexArray((GLuint)Mesh->VAO);
			CurrVAO = Mesh->VAO;
		}
		
		if(Mesh->EBO){
			glDrawElements(GL_TRIANGLES,Mesh->Count,GL_UNSIGNED_INT,0);
		}else{
			glDrawArrays(GL_TRIANGLES,0,Mesh->Count);
		}
	}
}

//Some utilities
inline void oBaWriteFloat(FILE *File){
	
}

inline void oBaWriteUInt(FILE *File){
	
}

inline float oBaFlipFloat(float Value){
	
}

inline uint32_t oBaInterpretUInt32(uint32_t Value,char LittleEndian){
	if(LittleEndian){
		return Value;
	}
	
	return ((Value & 0xff000000) >> 24) | ((Value & 0x00ff0000) >> 8) | ((Value & 0x0000ff00) << 8) | (Value << 24);
}