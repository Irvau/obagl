#include <stdint.h>

#include <stdio.h>
#include <math.h>

#include "oBagl.h"
#include "oBagl/Om.h"

#define HI_NIBBLE(Byte) ((Byte >> 4) & 0x0F)
#define LO_NIBBLE(Byte) (Byte & 0x0F)

char oBaIntReadChars[4];

inline uint32_t oBaOmReadInt(FILE *OmFile){
	fread(oBaIntReadChars,1,4,OmFile);
	
	return (oBaIntReadChars[0] << 24) + (oBaIntReadChars[1] << 16) + (oBaIntReadChars[2] << 8) + oBaIntReadChars[3];
}

float *oBaOmReadFloatArray(FILE *OmFile,size_t Count){
	float *FloatArray = malloc(sizeof(float) * Count);
	size_t FloatCount = 0;
	
	char Nibbles[2],IsCurrDec = 0;
	unsigned int Number[2];
	
	while(FloatCount < Count){
		fread(Nibbles,1,1,OmFile);
		
		Nibbles[1] = LO_NIBBLE(Nibbles[0]);
		Nibbles[0] = HI_NIBBLE(Nibbles[0]);
		
		for(size_t I = 0;I < 2;++I){
			if(Nibbles[I] == 10){
				IsCurrDec = 1;
			}else if(Nibbles[I] > 10){
				FloatArray[FloatCount] = Numbers[0] + (Numbers[1] / (pow(10,floor(log10(Numbers[1])) + 1)));
				
				++FloatCount;
				IsCurrDec = 0;
			}else{
				Number[IsCurrDec] = (Number[IsCurrDec] *  10) + Nibbles[I];
			}
		}
	}
	
	return FloatArray;
}

void oBaLoadOmFile(const char *Path,oBaOm Om,size_t OmCount,oBaArray Array,size_t ArrayCount){
	FILE *OmFile = fopen(Path,"rb");
	
	if(OmFile){
		//Checking the sig
		char Sig[4];
		fread(Sig,1,4,OmFile);
		
		if(Sig[0] == 5 && Sig[1] == 111 && Sig[2] == 109 && Sig[3] == 6){
			//Reading the counts
			char Counts;
			fread(&Counts,1,1,OmFile);
			
			char PropertyCount = HI_NIBBLE(Counts) + 1;
			char AttribCount = LO_NIBBLE(Counts) + 1;
			
			//Reading the property and attribute sizes
			char PropertySizes[PropertyCount],AttribSizes[AttribCount];
			
			char SizeByteCount = ceil((PropertyCount + AttribCount) / 4);
			char SizeBytes[AttribSizeByteCount];
			
			fread(SizeBytes,1,SizeByteCount,OmFile);
			
			for(size_t I = 0;I < PropertyCount + AttribCount;++I){
				if(I < PropertyCount){ //Property size byte parsing
					PropertySizes[I] = SizeBytes[floor(I / 4)] | (0xFF << ((I % 4) * 2) >> 6);
				}else{ //Attribute size byte parsing
					AttribSizes[I - PropertyCount] = SizeBytes[floor(I / 4)] | (0xFF << ((I % 4) * 2) >> 6);
				}
			}
			
			//Reading in the data blocks
			size_t DataBlockCount = oBaOmReadInt(OmFile);
			
			if(DataBlockCount){
				Block = malloc(sizeof(struct oBafArray) * DataBlockCount);
				
				for(size_t I = 0;I < DataBlockCount;++I){
					Block[I].Size = oBaOmReadInt(OmFile);
					Block[I].Data = oBaOmReadFloatArray(OmFile,Block[I].Size);
				}
			}
			
			//Reading in the objects
			size_t ReadOmCount = oBaOmReadInt(OmFile);
			
			if(OmCount && OmCount < ReadOmCount){
				ReadOmCount = OmCount;
			}
			
			oBaMesh *Meshes = malloc(sizeof(struct oBafOm) * ReadOmCount);
			
			for(size_t I = 0;I < ReadOmCount;++I){
				//Reading in the properties.
				Meshes[I].PropertyCount = PropertyCount;
				Meshes[I].Properties = malloc(sizeof(struct oBafArray) * PropertyCount);
				
				for(size_t J = 0;J < PropertyCount;++J){
					Meshes[I].Properties[J].Size = PropertySizes[J];
					Meshes[I].Properties[J].Data = oBaOmReadFloatArray(OmFile,PropertySizes[J]);
				}
				
				//Reading the attributes
				Meshes[I].AttribCount = AttribCount;
				Meshes[I].AttribVertCount = oBaOmReadInt(OmFile) + 1;
				Meshes[I].Attribs = malloc(sizeof(struct oBafArray) * AttribCount);
				
				for(size_t J = 0;J < AttribCount;++J){
					Meshes[I].Attribs[J].Size = AttribSizes[J];
					Meshes[I].Attribs[J].Data = malloc(sizeof(struct oBafArray));
					
					
				}
				
			}
		}else if(oBaDebugOutput){
			printf("\"%s\" is not a valid oBagl mesh file\n",Path);
		}
		
		fclose(Meshfile);
	}else if(oBaDebugOutput){
		printf("Unable to open \"%s\"\n",Path);
	}
}

oBaOm oBaCreateOms(size_t Count){
	
}

void oBaFreeOm(oBaOm Om){
	
}