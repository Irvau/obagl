#include <stdio.h>

#include <GL/glew.h>
#include <kazmath/mat4.h>

#include "oBagl.h"
#include "oBagl/Camera.h"
#include "oBagl/Entity.h"

unsigned int CurrFBO = 0;

struct oBafCamera{
	oBaEntity Entity;
	oBaScene Scene;
	
	unsigned int MatricesUBO;
	
	kmMat4 Projection;
	
	char ProjMode;
	char ProjUpdate;
	
	float FOV,Aspect,Zoom;
	float Width,Height;
	float Near,Far;
	
	unsigned int FBO,RBO;
	int Texture;
};

oBaCamera oBaCreateCamera(){
	oBaCamera Camera = malloc(sizeof(struct oBafCamera));
	
	Camera->Scene = NULL;
	
	Camera->MatricesUBO = 0;
	Camera->ProjMode = 0;
	Camera->ProjUpdate = 0;
	Camera->FOV = 0.0;
	Camera->Aspect = 0.0;
	Camera->Zoom = 1.0;
	Camera->Width = 0.0;
	Camera->Height = 0.0;
	Camera->Near = 0.0;
	Camera->Far = 0.0;
	Camera->FBO = 0;
	Camera->RBO = 0;
	Camera->Texture = 0;
	
	glGenBuffers(1,&Camera->MatricesUBO);
	
	glBindBuffer(GL_UNIFORM_BUFFER,Camera->MatricesUBO);
	glBufferData(GL_UNIFORM_BUFFER,sizeof(kmMat4),NULL,GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER,0);
	
	return Camera;
}

void oBaFreeCamera(oBaCamera Camera){
	glDeleteBuffers(1,&Camera->MatricesUBO);
	free(Camera);
}

oBaEntity oBaCameraEntity(oBaCamera Camera){
	return Camera->Entity;
}

void oBaCameraSetPerspective(oBaCamera Camera,float FOV,float Aspect,float Near,float Far){
	if(FOV > 0){
		Camera->FOV = FOV;
	}
	
	if(Aspect > 0){
		Camera->Aspect = Aspect;
	}
	
	if(Near > 0){
		Camera->Near = Near;
	}
	
	if(Far > 0){
		Camera->Far = Far;
	}
	
	Camera->Width = 0;
	Camera->Height = 0;
	
	Camera->ProjMode = PERSPECTIVE_PROJ;
	Camera->Projection = kmMat4PerspectiveProjection(Camera->FOV / Camera->Zoom,Camera->Aspect,Camera->Near,Camera->Far);
	
	Camera->ProjUpdate = 1;
}

void oBaCameraSetOrthographic(oBaCamera Camera,float Width,float Height,float Near,float Far){
	if(Width > 0){
		Camera->Width = Width;
	}
	
	if(Height > 0){
		Camera->Height = Height;
	}
	
	if(Near > 0){
		Camera->Near = Near;
	}
	
	if(Far > 0){
		Camera->Far = Far;
	}
	
	FOV = 0.0;
	Aspect = 0.0;
	
	Camera->ProjMode = ORTHOGRAPHIC_PROJ;
	Camera->Projection = kmMat4OrthographicProjection(Camera->Width * -0.5f / Zoom,Camera->Width * 0.5f / Zoom,Camera->Height * -0.5f / Zoom,Camera->Height * 0.5f / Camera->Zoom,Camera->Near,Camera->Far);
	
	Camera->ProjUpdate = 1;
}

int oBaCameraGetProjection(oBaCamera Camera){
	return Camera->ProjectionMode;
}

void oBaCameraAttachTexture(oBaCamera Camera,unsigned int Width,unsigned int Height){
	if(Camera->Target){
		oBaTextureFree(Camera->Texture);
	}
	
	Texture = oBaCreateTexture(Width,Height);
	
	glGenFramebuffers(1,&Camera->FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,Camera->FBO);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,oBaTextureName(Camera->Texture),0);
	
	glGenRenderbuffers(1,&Camera->RBO);
	glBindRenderbuffer(GL_RENDERBUFFER,Camera->RBO);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT32,Width,Height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,Camera->RBO);
	
	if(oBaDebugOutput && glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		printf("Framebuffer creation failed\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glBindRenderbuffer(GL_RENDERBUFFER,0);
}

oBaTexture oBaCameraGetTexture(oBaCamera Camera){
	return Camera->Texture;
}

void oBaCameraSetScene(oBaCamera Camera,oBaScene Scene){
	Camera->Scene = Scene;
}

oBaScene oBaCameraGetScene(oBaCamera Camera){
	return Camera->Scene;
}

void oBaCameraRender(oBaCamera Camera,char ToTexture = 0){
	if((!ToTexture) || (ToTexture && Camera->FBO)){
		unsigned int TargetFBO = ((ToTexture > 0) * (CurrFBO != Camera->FBO)) * Camera->FBO;
		
		if(CurrFBO != TargetFBO){
			glBindFramebuffer(GL_FRAMEBUFFER,TargetFBO);
			CurrFBO = TargetFBO;
		}
		
		glBindBufferBase(GL_UNIFORM_BUFFER,SHADER_MATRIX_UBO,Camera->MatricesUBO);
		
		glm::mat4 ProjectionView = Projection * glm::inverse(Transformations);
		
		oBaDrawScene(Camera->Scene);
	}
}

void oBaCameraSetZoom(oBaCamera Camera,float Zoom){
	Camera->Zoom = Zoom;
	
	switch(Camera->ProjMode){
		case UNINITIATED_PROJ:
			break;
		case PERSPECTIVE_PROJ:
			oBaCameraSetPerspective(Camera);
			break;
		case ORTHOGRAPHIC_PROJ:
			oBaCameraSetOrthographic(Camera);
			break;
	}
}

float oBaCameraGetZoom(oBaCamera Camera){
	return Camera->Zoom;
}