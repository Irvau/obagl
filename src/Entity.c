#include <kazmath/mat4.h>

#include "oBagl/Entity.h"

struct oBafEntity{
	float X,Y,Z;
	float RX,RY,RZ;
	float SX,SY,SZ;
	
	kmMat4 Transformations;
	
	oBaEntity Parent;
};

oBaEntity oBaCreateEntity(){
	oBaEntity Entity = malloc(sizeof(oBafEntity));
	
	Entity->X = 0.0;
	Entity->X = 0.0;
	Entity->X = 0.0;
	
	Entity->RX = 0.0;
	Entity->RX = 0.0;
	Entity->RX = 0.0;
	
	Entity->SX = 1.0;
	Entity->SX = 1.0;
	Entity->SX = 1.0;
	
	Entity->Parent = NULL;
	
	return Entity;
}

void oBaFreeEntity(oBaEntity Entity){
	free(Entity);
}

void oBaEntityPosition(oBaEntity Entity,float NewX,float NewY,float NewZ){
	Entity->X = NewX;
	Entity->Y = NewY;
	Entity->Z = NewZ;
	
	kmMat4Translation(Entity->Transformations,Entity->X,Entity->Y,Entity->Z);
}

void oBaEntityMove(oBaEntity Entity,float DiffX,float DiffY,float DiffZ){
	Entity->X += DiffX;
	Entity->Y += DiffY;
	Entity->Z += DiffZ;
	
	kmMat4Translation(Entity->Transformations,Entity->X,Entity->Y,Entity->Z);
}

void oBaEntityRotation(oBaEntity Entity,float NewRX,float NewRY,float NewRZ){
	Entity->RX = NewRX;
	Entity->RY = NewRY;
	Entity->RZ = NewRZ;
	
	kmMat4RotationYawPitchRoll(&Entity->Transformations,kmDegreesToRadians(Entity->RX),kmDegreesToRadians(Entity->RY),kmDegreesToRadians(Entity->RZ));
}

void oBaEntityRotate(oBaEntity Entity,float DiffRX,float DiffRY,float DiffRZ){
	Entity->RX += DiffRX;
	Entity->RY += DiffRY;
	Entity->RZ += DiffRZ;
	
	kmMat4RotationYawPitchRoll(&Entity->Transformations,kmDegreesToRadians(Entity->RX),kmDegreesToRadians(Entity->RY),kmDegreesToRadians(Entity->RZ));
}

void oBaEntitySetScale(oBaEntity Entity,float NewSX,float NewSY,float NewSZ){
	Entity->SX = NewSX;
	Entity->SY = NewSY;
	Entity->SZ = NewSZ;
	
	kmMat4Scaling(Entity->Transformations,Entity->SX,Entity->SY,Entity->SZ);
}

void oBaEntityScale(oBaEntity Entity,float DiffSX,float DiffSY,float DiffSZ){
	Entity->SX += DiffSX;
	Entity->SY += DiffSY;
	Entity->SZ += DiffSZ;
	
	kmMat4Scaling(Entity->Transformations,Entity->SX,Entity->SY,Entity->SZ);
}

float oBaEntityX(oBaEntity Entity){
	return Entity->X;
}

float oBaEntityY(oBaEntity Entity){
	return Entity->Y;
}

float oBaEntityZ(oBaEntity Entity){
	return Entity->Z;
}

float oBaEntityRX(oBaEntity Entity){
	return Entity->RX;
}

float oBaEntityRY(oBaEntity Entity){
	return Entity->RY;
}

float oBaEntityRZ(oBaEntity Entity){
	return Entity->RZ;
}

float oBaEntitySX(oBaEntity Entity){
	return Entity->SX;
}

float oBaEntitySY(oBaEntity Entity){
	return Entity->SY;
}

float oBaEntitySZ(oBaEntity Entity){
	return Entity->SZ;
}

oBaEntity oBaEntityGetParent(oBaEntity Entity){
	return Entity->Parent;
}

void oBaEntitySetParent(oBaEntity Entity,oBaEntity Parent){
	Entity->Parent = Parent;
}