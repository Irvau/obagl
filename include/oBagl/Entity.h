#ifndef OBA_ENTITY_INC
	typedef struct oBafEntity *oBaEntity;
	
	oBaEntity oBaCreateEntity();
	void oBaFreeEntity(oBaEntity Entity);
	
	void oBaEntityPosition(oBaEntity Entity,float NewX,float NewY,float NewZ);
	void oBaEntityMove(oBaEntity Entity,float DiffX,float DiffY,float DiffZ);
	
	void oBaEntityRotation(oBaEntity Entity,float NewRX,float NewRY,float NewRZ);
	void oBaEntityRotate(oBaEntity Entity,float DiffRX,float DiffRY,float DiffRZ);
	
	void oBaEntitySetScale(oBaEntity Entity,float NewSX,float NewSY,float NewSZ);
	void oBaEntityScale(oBaEntity Entity,float DiffSX,float DiffSY,float DiffSZ);
	
	float oBaEntityX(oBaEntity Entity);
	float oBaEntityY(oBaEntity Entity);
	float oBaEntityZ(oBaEntity Entity);
	
	float oBaEntityRX(oBaEntity Entity);
	float oBaEntityRY(oBaEntity Entity);
	float oBaEntityRZ(oBaEntity Entity);
	
	float oBaEntitySX(oBaEntity Entity);
	float oBaEntitySY(oBaEntity Entity);
	float oBaEntitySZ(oBaEntity Entity);
	
	oBaEntity oBaEntityGetParent(oBaEntity Entity);
	void oBaEntitySetParent(oBaEntity Entity,oBaEntity Parent);
	
	#define OBA_ENTITY_INC
#endif