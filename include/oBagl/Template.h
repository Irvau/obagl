#ifndef OBA_TEMPLATE_INC
	#include "oBagl/Mesh.h"
	#include "oBagl/Texture.h"
	
	#define OBA_MAX_TEMPLATE_TEXTURE_COUNT 8
	
	struct oBafTemplate{
		oBaMesh Mesh;
		
		oBaTexture Textures[OBA_MAX_TEMPLATE_TEXTURE_COUNT];
		unsigned int TexCount;
		
		unsigned int Shader;
	};
	
	typedef struct oBafTemplate *oBaTemplate;
	
	void oBaTemplateSetup();
	void oBaTemplateCleanUp();
	
	oBaTemplate oBaCreateTemplate();
	void oBaTemplateTextures(oBaTemplate Template,int Count,...);
	
	void oBaUseTemplate(oBaTemplate Template);
	
	#define OBA_TEMPLATE_INC
#endif