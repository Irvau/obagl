#ifndef OBA_MESH_INC
	typedef struct oBafMesh *oBaMesh;
	
	oBaMesh oBaLoadMesh(const char *Path);
	oBaMesh *oBaLoadMeshes(const char *Path,unsigned int ObjectCount = 0);
	
	void oBaFreeMesh(oBaMesh Mesh);
	void oBaFreeMeshes(oBaMesh *Meshes,unsigned int ObjectCount);
	
	void oBaDrawMesh(oBaMesh Mesh);
	
	#define OBA_MESH_INC
#endif