#ifndef OBA_CAMERA_INC
	#include "oBagl/Texture.h"
	#include "oBagl/Scene.h"
	#include "oBagl/Entity.h"
	
	#define OBA_CAMERA_PERSPECTIVE_PROJ 0
	#define OBA_CAMERA_ORTHOGRAPHIC_PROJ 1
	
	typedef struct oBafCamera *oBaCamera;
	
	oBaCamera oBaCreateCamera();
	void oBaFreeCamera(oBaCamera Camera);
	
	oBaEntity oBaCameraEntity(oBaCamera Camera);
	
	void oBaCameraSetPerspective(oBaCamera Camera,float FOV = -1,float Aspect = -1,float Near = -1,float Far = -1);
	void oBaCameraSetOrthographic(oBaCamera Camera,float Width = -1,float Height = -1,float Near = -1,float Far = -1);
	int oBaCameraGetProjection(oBaCamera Camera);
	
	void oBaCameraAttachTexture(oBaCamera Camera,unsigned int Width,unsigned int Height);
	oBaTexture oBaCameraGetTexture(oBaCamera Camera);
	
	void oBaCameraSetScene(oBaCamera Camera,oBaScene Scene);
	oBaScene oBaCameraGetScene(oBaCamera Camera);
	
	void oBaCameraRender(oBaCamera Camera,char ToTexture = 0);
	
	void oBaCameraSetZoom(oBaCamera Camera,float Zoom);
	float oBaCameraGetZoom(oBaCamera Camera);
	
	#define OBA_CAMERA_INC
#endif