#ifndef OBA_SCENE_INC
	#include "oBagl/Object.h"
	
	#define OBA_SCENE_MAX_LIGHTS 64;
	
	typedef oBafScene *oBaScene;
	
	oBaScene oBaCreateScene();
	void oBaFreeScene(oBaScene Scene);
	
	void oBaSceneAddObject(oBaScene Scene,oBaObject Object);
	oBaObject oBaSceneCreateObject(oBaScene Scene,oBaTemplate Template = NULL,oBaEntity Entity = NULL);
	
	void oBaSceneRemoveObject(oBaScene Scene,oBaObject Object);
	void oBaSceneDestroyObject(oBaScene Scene,oBaObject Object);
	
	void oBaDrawScene(oBaScene Scene);
	
	#define OBA_SCENE_INC
#endif