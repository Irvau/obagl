#ifndef OBA_TEXTURE_INC
	typedef struct oBafTexture *oBaTexture;
	
	oBaTexture oBaCreateTexture(unsigned int Width = 0,unsigned int Height = 0);
	oBaTexture oBaLoadTexture(const char *Path);
	void oBaFreeTexture(oBaTexture Texture);
	
	unsigned int oBaTextureName(oBaTexture Texture);
	unsigned int oBaTextureWidth(oBaTexture Texture);
	unsigned int oBaTextureHeight(oBaTexture Texture);
	
	#define OBA_TEXTURE_INC
#endif