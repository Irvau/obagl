#ifndef OBA_SHADER_INC
	#define OBA_SHADER_MATRIX_UBO 0
	#define OBA_SHADER_LIGHTS_UBO 1
	
	void oBaUseShader(unsigned int Shader);
	unsigned int oBaCurrentShader();
	
	unsigned int oBaLoadShaderFromFiles(const char *VertexPath,const char *FragmentPath);
	unsigned int oBaLoadShaderFromSources(const char *VertexSource,const char *FragmentSource);
	void oBaFreeShader(unsigned int Shader);
	
	#define OBA_SHADER_INC
#endif