#ifndef OBA_OM_INC
	/*struct oBafArray{
		size_t Size;
		void *Data;
	};
	
	struct oBafOm{
		size_t PropertyCount,AttribCount,AttribVertCount;
		size_t VertCount,Verts;
		
		oBaArray Properties,Attribs;
	};*/
	
	struct oBafOm{
		size_t PropertyCount;
		char *PropertySizes;
		float *PropertyData;
		
		size_t AttribCount,AttribVertCount;
		char *AttribSizes,*AttribStates;
		float **AttribData;
		
		size_t VertCount;
		unsigned int *VertData;
	};
	
	typedef struct oBafArray *oBaArray;
	typedef struct oBafOm *oBaOm;
	
	void oBaLoadOmFile(const char *Path,oBaOm Om,size_t OmCount,oBaArray Array,size_t ArrayCount);
	
	oBaOm oBaCreateOms(size_t Count);
	void oBaFreeOm(oBaOm Om);
	
	#define OBA_OM_INC
#endif