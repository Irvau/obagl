#ifndef OBA_OBJECT_INC
	#include "oBagl/Entity.h"
	#include "oBagl/Template.h"
	
	struct oBafObject{
		oBaEntity Entity;
		oBaTemplate Template;
	};
	
	typedef struct oBafObject *oBaObject;
	
	oBaObject oBaCreateObject(oBaTemplate Template = NULL,oBaEntity Entity = NULL);
	void oBaFreeObject(oBaObject Object,char FreeEntity = 1);
	
	#define OBA_OBJECT_INC
#endif