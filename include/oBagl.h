#ifndef OBAGL_INC
	#include "oBagl/Camera.h"
	#include "oBagl/Entity.h"
	#include "oBagl/Mesh.h"
	#include "oBagl/Object.h"
	#include "oBagl/Scene.h"
	#include "oBagl/Shader.h"
	#include "oBagl/Texture.h"
	#include "oBagl/Template.h"
	
	extern char oBaDebugOutput;
	
	void oBaInit();
	
	#define OBAGL_INC
#endif